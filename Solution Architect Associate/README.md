# Solution Architect Associate #

These are my notes of the acloud.guru course you can find here :  [Certified Solutions Architect: Associate 2017](https://acloud.guru/course/aws-certified-solutions-architect-associate/dashboard)

Click => [Certified Solutions Architect - Associate 2017 Notes/PDF](Certified Solutions Architect - Associate 2017.pdf)

_For an excellent breakdown of the topics see [Carlo Carbone's](https://bitbucket.org/carlocarbone/aws-certification-study-notes) notes._
