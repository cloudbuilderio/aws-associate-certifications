# Highy Availability & Fault Tolerance

## Overview

## Key Points
- AWS provides services, infrastructure to build reliable, fault-tolerant, highly available systems in the cloud.
- Most of the higher-level services, such as `S3`, `SimpleDB`, `SQS`, and `ELB`, have been built with fault tolerance and high availability in mind.
- Services that provide basic infrastructure, such as `EC2` and `EBS`, provide specific features, such as **availability zones**, **elastic IP addresses**, and **snapshots**, that a fault-tolerant and highly available system must take advantage of and use correctly.

#### Regions & Availability Zones
- It is important to run independent application stacks in more than one AZ, either in the same region or in another region, so that if one zone fails, the application in the other zone can continue to run.

#### AMI's - EC2 (high-availability)
- Amazon Machine Image (AMI) provides a Template that can be used to define the service instances.
- A single AMI can be used to create server resources of different instance types and start creating new instances or replacing failed instances

#### Auto Scaling - EC2 (fault-tolerant)
- automatically scales EC2 up & down
- can work across Multi-AZ within a Region

#### ELB - EC2 (high-availability)
- used with Auto Scaling is ideal combination
- support health checks
- distributes load/traffic across Multi-AZ
- dynamic load/unload EC2 hosts

#### Elastic IP
- public static IP
- associated with **AWS Account** not the instance, or lifetime of instance
- EIP addresses can be used for instances and services that require consistent endpoints, such as, `master databases`, `central file servers`, and `EC2-hosted load balancers`

- EIP addresses can be used to work around host or availability zone failures by quickly remapping the address to another running instance or a replacement instance that was just started.

#### Reserved Instance
- guarantees computing capacity is available (at lower cost too).

#### EBS
- offers persistent off-instance storage volumes that persists independently from the life of an instance
- store data `redundantly` and are `automatically replicated` within a `single availability zone`.
- **failover:** can be detached from failed instance, and attached to the new one.

#### EBS Snapshots
- point-in-time Snapshots can be created to store data on volumes in S3, which is then replicated to multiple AZs
- Snapshots are incremental and back up only changes since the previous snapshot, so it is advisable to hold on to recent snapshots
- Snapshots are tied to the region, while EBS volumes are tied to a single AZ

#### S3
- highly durable, fault-tolerant, redundant object store.
- unlimited object file storage
- remember good for: (objects) images, videos, static media
- support EDGE CACHING, streaming these assets via CloudFront.

#### Route53
- a highly available and scalable DNS web service.

#### CloudFront
- is a CDN (content delivery network)
- content automatically routed to nearest EDGE LOCATION
- optimized to work with S3, EC2
- works well with NON-AWS origin servers.

####

### Questions look like...


### References:
