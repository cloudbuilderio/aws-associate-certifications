# S3

## Overview
Amazon S3 is storage for the Internet. It’s a simple storage service that offers software developers a highly-scalable, reliable, and low-latency data storage infrastructure at very low costs.

## Key Points

* [ACLs](http://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html)
  * policy permissions
  * syntax gotchas:
    ```
    “Statement”: [{
      “Sid”: “Stmt1388811069831”,
      “Effect”: “Allow”,
      “Principal”: { “AWS”: “*”},
      “Action”: [ “s3:GetObjectAcl”, “s3:ListBucket”],
      “Resource”: [ “arn:aws:s3:::demo]
    }]
    ```
    Resource reference incorrect:
    "Actions" has 2 iteams, GetObjectAcl and ListBucket. The "Resource" references "arn:aws:s3:::demo" which is a bucket.
    - s3:ListBucket will work on the Resource because its a bucket
    - s3:GetObjectAcl will NOT WORK on the Resource because it applies to OBJECTS INSIDE THE BUCKET.
    correct syntax would be `“Resource”: [ “arn:aws:s3:::demo/*]`

* provides
  * unlimited object storage
    * highly-scalable, low-latency

* min/max/uploads
  * Multi-Part uploads
    * Improved throughput—you can upload parts in parallel to improve throughput.
    * Quick recovery from any network issues—smaller part size minimizes the impact of restarting a failed upload due to a network error.
    * Pause and resume object uploads—you can upload object parts over time. Once you initiate a multipart upload there is no expiry; you must explicitly complete or abort the multipart upload.
    * Begin an upload before you know the final object size—you can upload an object as you are creating it.
  * http://docs.aws.amazon.com/AmazonS3/latest/dev/qfacts.html

* Lifecycle
  * rollback window for objects

* Analytics

### Questions look like...
> It is possible to have a rollback windows for objects in S3. If yes, then which of the below methods can help achieve this

> Which feature in S3 allows one the identify access patterns whilst using the storage in S3.

> From the below options which are true with regards to AWS S3. Choose 2 answers from the options given below.

> What step(s) would ensure that the file uploads are able to complete in the allotted time window?

> Which of the below mentioned policy permissions is equivalent to the WRITE ACL on a bucket?

### Reference:
* https://aws.amazon.com/s3/faqs/
