# VPC

## Overview

## Key Points

* [default VPC](http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/default-vpc.html#default-vpc-components)
  * by default, the 'default VPC' will have:
    - /16 CIDR block
    - 2 /20 sized Subnets (1 in each AZ)
    - 1 IGW attached to VPC (max:1)
    - 1 Route Table
      * rule to send all IPv4 internet traffic via the IGW
    - 1 Security Group
    - 1 NACL

    * instances (e.g. EC2) deployed in VPC subnets get both a PUBLIC and PRIVATE IP.

* [custom VPC](http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Scenario1.html)
  *

* [network ACLs](http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_ACLs.html)

* IP - public/private/Elastic
  * If you want to launch Amazon Elastic Compute Cloud (EC2) Instances and assign each Instance a Predetermined private IP address you should launch the instances in the Amazon virtual Private Cloud (VPC)

* [Subnets](http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Subnets.html)
  * The first four IP addresses and the last IP address in each subnet CIDR block are not available for you to use, and cannot be assigned to an instance. For example, in a subnet with CIDR block 10.0.0.0/24, the following five IP addresses are reserved:

  	* 10.0.0.0: Network address.
  	* 10.0.0.1: Reserved by AWS for the VPC router.
  	* 10.0.0.2: Reserved by AWS. The IP address of the DNS server is always the base of the VPC network range plus two; however, we also reserve the base of each subnet range plus two. 10.0.0.3: Reserved by AWS for future use.
  	* 10.0.0.255: Network broadcast address. We do not support broadcast in a VPC, therefore we reserve this address.

  * When you create a subnet with the default settings:
    - only the Private IP gets populated for EC2 instances.
    - For Public IP, this is not possible because the Auto-assign Public IP will be ‘no’ by default.
    - Also the Elastic IP and Internet gateway have to manually configured.
      * i.e. by default for simple subnet, you'd have to create an IGW and attach an elastic IP to be able to connect from the Internet.

  * Delete a Subnet:
    * you cannot delete a subnet until all instances in it are TERMINATED.
    * VPC wizard creates a NAT instance, be aware when asked this in exam - an instance EXISTS, so will error if you try to delete it.


* [Security Groups](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-network-security.html#default-security-group)
  * Default Security groups
    * Your AWS account automatically has a default security group per VPC and per region for EC2-Classic.
    * If you don't specify a security group when you launch an instance, the instance is automatically associated with the default security group.
    * A default security group is named `default`, and it has an ID assigned by AWS.
    * The following are the default rules for each default security group:
      * Allows all inbound traffic from other instances associated with the default security group (the security group specifies itself as a source security group in its inbound rules)
      * Allows all outbound traffic from the instance.
      * You can add or remove the inbound rules for any default security group.
      * You can add or remove outbound rules for any VPC default security group.
      * You can't delete a default security group.

    > If you try to delete the EC2-Classic default security group, you'll get the following error: `Client.InvalidGroup.Reserved: The security group 'default' is reserved`. If you try to delete a VPC default security group, you'll get the following error: `Client.CannotDelete: the specified group: "sg-51530134" name: "default" cannot be deleted by a user`.


* failover - ENI's

* bastion host - highly available (Configure the bastion instance in an Auto Scaling group Specify the Auto Scaling group to include multiple AZs but have a min-size of 1 and max-size of 1 [Correct])


### Questions look like...
> Which of the following is the best method to quickly and temporarily deny access from the specified IP Address's.

> Which two things do we need to confirm in the VPC settings so that these EC2 instances can communicate inside the VPC? Choose 2 answers.

> Which two mechanisms will allow the application to failover to new instances without the need for reconfiguration? Choose 2 answers


### Reference:
	* http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_ACLs.html
  * https://aws.amazon.com/vpc/
