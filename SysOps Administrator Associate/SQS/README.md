# SQS

## Overview
Amazon Simple Queue Service (SQS) is a fast, reliable, scalable, fully managed message queuing service. Amazon SQS makes it simple and cost-effective to decouple the components of a cloud application. You can use Amazon SQS to transmit any volume of data, without losing messages or requiring other services to be always available. Amazon SQS includes standard queues with high throughput and at-least-once processing, and FIFO queues that provide FIFO (first-in, first-out) delivery and exactly-once processing.

### SQS Stand Queue Features & Key Points

#### Redundant Inftrastructure

#### At-Least-Once delivery

#### Message Attributes
  - SQS message up to 10 metadata attributes
  - name+type+value form
  - separates body of message from metadat describing it
  - process & store information with greater speed, efficiency because dont have to inspect ENTIRE message before understanding how to process it.

#### Messages
  - short polling (default)
    * samples only subset of servers (weighted random distribution), gets messages from just those servers.
  - long polling
    * requests presist for a period of time, returns soon as message is available. reduces cost and time message waiting queue.

#### Batching
  - allows send, receive, delete batching up to 10 messages in a batch, while charging for a single message.
  - lower cost, increase throughput

#### Configure settings per Queue

#### Order
  - NO guarantee messages processed in order received - makes best effort but not guaranteed. (see FIFO)
  - ordering in standard can be done by placing sequencing information in the message, and the client side manages the order.

#### Loose Coupling.

#### Multiple writers & readers
  - multiple readers and writers interacting with the queue at the same time
  - locks message during processing using VISIBILITY TIMEOUT so other reader/writers can't process it.

#### Message Size
  - message = any format up to 256KB
  - messages > 256KB can be managed by S3 or DynamoDB with SQS holding a "pointer" to the S3 object.

#### Access Control
  - can control who produces & consumes the queues.

#### Delay Queues
  - user can set a 'default delay' on the queue so queued messages must wait for that time before delivery

#### Dead Letter Queue
  - Queue for messages no able to be processed after max number of attempts

#### PCI Compliance
  - has been validated as being PCI-DSS (Payment Card Industry - Data Security Standards) to support processing, storing, transmitting credit card data by merchant or service provider.

#### Queue URL endpoint format:
  * https://sqs.AZ.amazonaws.com/ResourceID/Queuename

* Message retention period = min: 1min, max: 14days
* Default message retention setting = 4 days

### SQS FIFO (new)
> FIFO queues have essentially the same features as standard queues, but provide the added benefits of supporting ordering and exactly-once processing. FIFO queues provide additional features that help prevent unintentional duplicates from being sent by message producers or from being received by message consumers. Additionally, message groups allow multiple separate ordered message streams within the same queue.  

- available in the US West (Oregon), US East (Ohio), US East (N. Virginia), and EU (Ireland) regions.
- need the latest SDK
- are limited to 300 transactions per second (TPS) per API action
- have all the capabilities of standard queues.
- compatible with: CloudWatch, S3 event notification, SNS Topic Subscriptions, Auto Scaling LFC Hooks, IoT Rules Actions, Lambda DLQ.

### SQS Use Cases:

#### Work Queues
  - decouple components of distributed application

#### Buffer & Batch Operations
  - scalability & reliability by smoothing out spikes without losing messages or increasing latency.

#### Request Offloading
  - move slow operations out of the way

#### Fan-out
  - combine SQS and SNS to send identical copies of messages to multiple queues to do parallel processing

#### Auto Scaling
  - SQS can determine load on application, and combine with Auto Scaling , the EC2 instances can be SCALED IN or OUT, depending on traffic volume.


### Questions look like...
> The user wants to decouple the data sending such that the application keeps processing and sending data but does not wait for an acknowledgement of DB. Which of the below mentioned applications helps in this scenario?


### Reference:
* https://aws.amazon.com/sqs/
* https://aws.amazon.com/sqs/faqs/
