# Auto Scaling

## Overview

Auto Scaling helps you ensure that you have the correct number of Amazon EC2 instances available to handle the load for your application.
You create collections of EC2 instances, called `Auto Scaling groups`. You can specify the minimum number of instances in each Auto Scaling group, and Auto Scaling ensures that your group never goes below this size. You can specify the maximum number of instances in each Auto Scaling group, and Auto Scaling ensures that your group never goes above this size.
If you specify the desired capacity, either when you create the group or at any time thereafter, Auto Scaling ensures that your group has this many instances.
If you specify scaling policies, then Auto Scaling can launch or terminate instances as demand on your application increases or decreases.

## Key Points

### [Scheduled Scaling](http://docs.aws.amazon.com/autoscaling/latest/userguide/schedule_time.html)
  To configure your Auto Scaling group to scale based on a schedule, you create a scheduled action, which tells Auto Scaling to perform a scaling action at specified times. To create a scheduled scaling action, you specify the start time when you want the scaling action to take effect, and the new minimum, maximum, and desired sizes for the scaling action. At the specified time, Auto Scaling updates the group with the values for minimum, maximum, and desired size specified by the scaling action.

### Scheduling Conflict
  Auto Scaling will throw an error since there is a conflict in the schedule of two separate Auto Scaling Processes

### Scheduling for Predictable Loads
  Auto Scaling based on a schedule allows the user to scale the application in response to predictable load changes.
  The user can specify any date in the future to scale up or down during that period.
  As per Auto Scaling the user can schedule an action for up to a month in the future.
  E.g. it is recommended to wait until end of November before scheduling for Christmas. 

### [Suspending and Resuming Auto Scaling Processes](http://docs.aws.amazon.com/autoscaling/latest/userguide/as-suspend-resume-processes.html)
  Auto Scaling enables you to suspend and then resume one or more of the Auto Scaling processes in your Auto Scaling group.
  This can be useful when you want to investigate a configuration problem or other issue with your web application and then make changes to your application, without triggering the Auto Scaling process.

  *Auto Scaling might suspend processes for Auto Scaling groups that repeatedly fail to launch instances*. This is known as an `administrative suspension`, and most commonly applies to Auto Scaling groups that have been trying to _launch instances for over 24 hours but have not succeeded in launching any instances_. You can resume processes suspended for administrative reasons.

  If you suspend `AlarmNotification`, Auto Scaling does not automatically execute policies that would be triggered by an alarm. If you suspend Launch or Terminate, Auto Scaling would not be able to execute scale out or scale in policies, respectively.

### [Auto Scaling Lifecycle](http://docs.aws.amazon.com/autoscaling/latest/userguide/AutoScalingGroupLifecycle.html)

  * autoscaling performs:
    - Launch
    - Terminate
    - Healthcheck
    - ReplaceUnhealthy
    - AZRebalance
    - AlarmNotification
    - ScheduledActions
    - AddToLoadBalancer

### CLI
  use `update-auto-scaling-group` for merging groups to span multiple zones.

  - enabling Auto Scaling via CLI, `detailed monitoring` (1 min intervals) will be enabled by default.

  http://docs.aws.amazon.com/cli/latest/reference/autoscaling/update-auto-scaling-group.html
  

### [Launch Configuration](http://docs.aws.amazon.com/autoscaling/latest/userguide/LaunchConfiguration.html)

A launch configuration is a template that an Auto Scaling group uses to launch EC2 instances.
When you create a launch configuration, you specify information for the instances such as the `ID of the Amazon Machine Image (AMI)`, the `instance type`, a `key pair`, one or more `security groups`, and a `block device mapping`.


### Questions look like...
> A user has created a web application with Auto Scaling. The user is regularly monitoring the application and he observed that the traffic is highest on Thursday and Friday between 8 AM to 6 PM. What is the best solution to handle scaling in this case?

### Exam Tip:


### Reference:
[AutoScaling](https://aws.amazon.com/autoscaling/)
