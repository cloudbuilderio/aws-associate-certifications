# SysOps Administrator Associate #

## Disclaimer
_**It is assumed you have already completed the SOLUTION ARCHITECT and DEVELOPER ASSOCIATE exams before using these notes as they DO NOT cover those topics in-depth. These notes are for sitting the SYSOPS ADMINISTRATOR exam only**_

See my other folders for notes on Solution Architect and Developer Exams. For some really good notes on Solution Architect & Developer see: [Carlo Carbone's](https://bitbucket.org/carlocarbone/aws-certification-study-notes) study notes.


_note: this is a work in progress and will be updated as required._

# SysOps Topic Structure (ordered by exam emphasis)
1. [Compute - EC2](EC2/README.md)
2. [Networking - VPC](VPC/README.md)
3. [Scalability - ELB & AutoScaling](ELB/README.md)
4. Configuration Management - AMI, Snapshot, UserData (see EC2)
5. [Tagging & Billing](BillingAndFreeTier/README.md)
6. Storage & Archiving - EBS, S3, Glacier, Instance Storage (See EC2, S3)
7. [Monitoring - CloudWatch](CloudWatch/README.md)
8. [Automation - CloudFormation](CloudFormation/README.md)
9. AWS CLI (TBC)
10. [Security - EC2, S3, RDS](Security/README.md)


# Table of Contents (alphabetical)
1. [AutoScaling](AutoScaling/README.md)
2. [Billing & Free Tier](BillingAndFreeTier/README.md)
3. [CloudFormation - Automated Infrastructure Deployment](CloudFormation/README.md)
4. [CloudWatch - Monitoring & Metrics](CloudWatch/README.md)
5. [EC2 - Elastic Cloud Compute, EBS, AMI](EC2/README.md)
6. [ELB - Elastic Beanstalk](ELB/README.md)
7. [IAM - Identity Access Management](IAM/README.md)
8. [RDS - Relational Database Systems, Multi-AZ Deployments](RDS/README.md)
9. [S3 - Simple Storage Services](S3/README.md)
10. [AWS Shared Responsibility Model, Compliance](SharedResponsibilityModel/README.md)
11. [SQS - Simple Queue Service](SQS/README.md)
12. [VPC - Virtual Private Cloud, DirectConnect, VPN](VPC/README.md)
