# CloudWatch

## Overview
* AWS CloudWatch monitors AWS resources and applications in real-time.
* CloudWatch can be used to collect and track metrics, which are the variables to be measured for resources and applications.
* CloudWatch alarms can be configured
  - to send notifications or
  - to automatically make changes to the resources based on defined rules
* In addition to monitoring the built-in metrics that come with AWS, custom metrics can also be monitored
* CloudWatch provides system-wide visibility into resource utilization, application performance, and operational health.
* By default, CloudWatch stores the log data indefinitely, and the retention can be changed for each log group at any time
* CloudWatch Alarm history is stored for only 14 days

## Architecture
![CloudWatch Diagram](CW-Overview.png)

## Key Points

* [Metrics](http://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/CW_Support_For_AWS.html)
  * key points:

      - Metric is the fundamental concept in CloudWatch.
      - Uniquely defined by a name, a namespace, and one or more dimensions
      - Represents a time-ordered set of data points published to CloudWatch.
      - Each data point has a time stamp, and (optionally) a unit of measure
      - Data points can be either custom metrics or metrics from other services in AWS.
      - Statistics can be retrieved about those data points as an ordered set of time-series data that occur within a specified time window.
      - When the statistics are requested, the returned data stream is identified by namespace, metric name, dimension, and (optionally) the unit.
      - Metrics exist only in the region in which they are created
      - CloudWatch stores the metric data for two weeks
      - Metrics cannot be deleted, but they automatically expire in 14 days if no new data is published to them.

  * Host Level Metrics
    - CPU
    - Network
    - Disk
    - Status Check

  * Metrics stored (default) 2 Weeks. Can get older than 2 weeks with `GetMetricStatistics` API, or 3rd party tools by AWS partners. Can get data from terminated EC2 or ELB instance up to 2 weeks after termination.

  * EBS backed EC2, no additional device attached:
    By default there would always be some NetworkIn , NetworkOut , CPU Utilization. But the DiskRead(Bytes) would remain 0.

  * aggregate metric data
    * If you go to Cloudwatch and go to any metric and see the statistic column you will see the below dimensions for statistics. And the only one which is not there from the above list is Aggregate.

* [Namespaces](namespaces.html)
  * namespaces are containers for metrics
  * metrics in different namespaces are isolated from each other to prevent differnt applications mistakenly aggregating the same statistics.
  * namespace convention: AWS/<service> e.g. `AWS/EC2` or `AWS/ELB`
  * namespace names < 256 characters

* [Aggregation](page.html)

  - CloudWatch aggregates statistics according to the period length specified in calls to `GetMetricStatistics`.
  - Multiple data points can be published with the same or similar time stamps. CloudWatch aggregates them by period length when the statistics about those data points are requested.
  - Aggregated statistics are only available when using detailed monitoring (1min).
  - Instances that use basic monitoring are not included in the aggregates
  - **CloudWatch does not aggregate data across regions. Metrics are completely separate between regions.**


* [Alarms & Notification](page.html)
  > Alarms can create ACTIONS for example if you want to launch a new instance when an EC2 server fails, you can create an ALARM to call SNS which has a trigger thatn can launch a new instance (e.g. in the private cloud/VPC)

  - Alarms can automatically initiate actions on behalf of the user, based on specified parameters
  - Alarm watches a single metric over a specified time period, and performs one or more actions based on the value of the metric relative to a given threshold over a number of time periods.
  - Alarms invoke actions for sustained state changes only i.e. the state must have changed and been maintained for a specified number of periods
  - Action can be a
    * SNS notification
    * Auto Scaling policies
    * EC2 action – stop or terminate EC2 instances
  - After an alarm invokes an action due to a change in state, its subsequent behavior depends on the type of action associated with the alarm.
    * For Auto Scaling policy notifications, the alarm continues to invoke the action for every period that the alarm remains in the new state.
    * For SNS notifications, no additional actions are invoked.

  * [Alarm Statuses](http://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/AlarmThatSendsEmail.html):
    * `OK` —The metric is within the defined threshold
    * `ALARM` —The metric is outside of the defined threshold
    * `INSUFFICIENT_DATA` —The alarm has just started, the metric is not available, or not enough data is available for the metric to determine the alarm state

    * example scenario in exam:
    > A user has setup an EBS backed instance and a CloudWatch alarm when the CPU utilization is more than 65%.
    The user has setup the alarm to watch it for 5 periods of 5 minutes each.
    The CPU utilization is 60% between 9 AM to 6 PM. The user has stopped the EC2 instance for 15 minutes between 11 AM to 11:15 AM.
    What will be the status of the alarm at 11:30 AM?

    * Answer is **"Alarm will be OK"**. Why?
    > Since the alarm has been set for 5 periods of 5 minutes each, that means if there is no activity for 25 minutes then the state of the Alarm will change.
    Now since the CPU utilization was 60%, that means the `alarm was not triggered and the state should be OK.`
    And since the instance was just stopped for 15 minutes and did not exceed the threshold for 25 minutes that means the state of the alarm stays the same.

  - Alarms exist only in the region in which they are created.
  - Alarm actions must reside in the same region as the alarm
  - Alarm history is available for the last 14 days.
  - Alarm can be tested by setting it to any state using the `SetAlarmState` API (`mon-set-alarm-state` command).
    * This temporary state change lasts only until the next alarm comparison occurs.
  - Alarms can be disabled and enabled using the `DisableAlarmActions` and `EnableAlarmActions` APIs (`mon-disable-alarm-actions` and `mon-enable-alarm-actions` commands).

  * increase billing alarm limit:
    * Let’s assume that an alarm has been created as shown below for any amounts exceeding 200 USD. To increase the limit, all you have to do is to click on the Modify option and you can change the value of the alarm in the next screen.

  * cloudwatch cli (set-alarm-state)

    - `disable-alarm-actions --alarm-names myalarm` - You can use the below CLI command to disable a cloudwatch alarm. When an alarm's actions are disabled, the alarm actions do not execute when the alarm state changes.


* [Custom Metrics](http://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/publishingMetrics.html)
  * CloudWatch allows you to publish your own metrics to CloudWatch using the AWS CLI or an API by supplying a file as an input to the CloudWatch command
    - e.g using `put-metric-data` CLI or Query API equivalent `PutMetricData`

    ```bash
    AWS cloudwatch put-metric-data --metric-name PageViewCount --namespace MyService --value 2 --timestamp 2016-10-14T12:00:00.000Z

    AWS cloudwatch get-metric-statistics --namespace MyService --metric-name PageViewCount \
      --statistics "Sum" "Maximum" "Minimum" "Average" "SampleCount" \
      --start-time 2016-10-20T12:00:00.000Z --end-time 2016-10-20T12:05:00.000Z --period 60
    ```
    - cloudwatch will create a new metric if `put-metric-data` is called with a NEW metric name, otherwise associates with existing metric.
      - new metric take up to 2 mins before statistics can be retrieved using `get-metric-statistics`
      - new metric take up to 15 mins before appears to the `list-metrics` command

    - `put-metric-data` can only public ONE DATA POINT per CALL.
    - cloudwatch stores data as a SERIES OF DATA POINT and each data point has an associated TIMESTAMP.
  * CloudWatch can publish:
    - Single Data Points
      - with timestamps as granular as 1/1000 of a second, aggregated to minimum granularity of ONE MINUTE.
      - cloudwatch records average of values received for every 1-minute period - along with # samples, max value, min value for the same period.
      - cloudwatch uses one-minute boundaries when aggregating data points.
    - Aggregated set of data points called _**'statistics set'**_
      - can aggregate your data before publishing to CloudWatch
      - aggregating minimizes # of calls needed to a single call per minute sending a 'statistics set'.
      - statistics include: `sum, average, maximum, minimum, data sample`.
  * Publishing ZERO (0)
    - if your app is sporadic and/or has periods where nothings happening, you can either publish a '0' value, or send nothing.
      * PRO TIP: its more helpful to public a '0' than no value at all, so SOME kind of value is recorded for tracking purposes e.g. total number of data points, and calculating minimum and average to include data points of '0'.



#### CloudWatch can be accessed using
- AWS CloudWatch console
- CloudWatch CLI
- AWS CLI
- CloudWatch API
- AWS SDKs


### misc. notes
* [Graphs](http://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/switch_graph_axes.html)
  * You can set custom bounds for the Y axis on a graph to help you see the data better. You can switch between two different Y axes for your graph by using `'Switch'` command. This is particularly useful if the graph contains metrics that have different units or that differ greatly in their range of values.

  * zoom into data points = _"The user can zoom a particular period by selecting that period with the mouse and then releasing the mouse"_

  * You can share an graph in Cloudwatch from the Share Graph URL option in cloudwatch.

### Questions look like...
> A user has setup a CloudWatch alarm on an EC2 action when the CPU utilization is above 75%. The alarm sends a notification to SNS on the alarm state. If the user wants to simulate the alarm action how can he achieve this?


### Exam Tip:
- RAM utilization is a custom metric! By default EC2 monitoring is 5 minute intervals, unless you enable detailed monitoring which is 1 minute intervals
- custom metrics minimum granularity you can have is 1 minute.

### Reference:
https://aws.amazon.com/cloudwatch/
