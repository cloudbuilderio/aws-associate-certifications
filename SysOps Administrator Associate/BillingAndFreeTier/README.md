# Billing & Free Tier

## Overview

AWS Billing and Cost Management is the service that you use to pay your AWS bill, monitor your usage, and budget your costs.

The service automatically charges the credit card you provided when you signed up for a new account with AWS.
Charges appear on your credit card bill monthly.
You can view or update credit card information, and designate a different credit card for AWS to charge, on the **Payment Methods** page in the `Billing and Cost Management console`.


## Key Points

### [Free Tier & Limits](https://aws.amazon.com/free/)

All services that offer a free tier have limits on what you can use without being charged. Many services have multiple types of limits.
For example, Amazon EC2 has limits on both the type of instance you can use, and how many hours you can use in one month. Amazon S3 has a limit on how much memory you can use, and also on how often you can call certain operations each month.
For example, the free tier covers the first 20,000 times you retrieve a file from Amazon S3, but you are charged for additional file retrievals.
Each service has limits that are unique to that service.

Some of the most common limits are by time, such as hourly or by the minute, or by requests, which are the requests you send to the service, also known as API operations.

|  Services | Limit   |
|---|---|
| EC2  | 750 hrs  |
| RDS  | 750 hrs  |
| S3  | 5 GB  |
| Lambda  | 1 Million requests/month  |
| SNS  | 1 Million publishes  |
| CloudWatch  | 10 custom metrics + 10 alarms  |


  _note: you get charged for EC2 instances ONLY when they are in 'running' state_

Check "Price of Services"[here](https://aws.amazon.com/pricing/services/)


### [Billing Reports](http://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/billing-reports.html)

  Billing reports provide information about your usage of AWS resources and estimated costs for that usage.
  You can have AWS generate billing reports that break down your estimated costs in different ways:

    * By the hour, day, or month
    * By each account in your organization
    * By product or product resource
    * By tags that you define yourself


### [Consolidating Billing](http://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/consolidated-billing.html)

AWS treats all the accounts on the consolidated bill as if they were one account.
Some services, such as Amazon EC2 and Amazon S3, have volume pricing tiers across certain usage dimensions that give you lower prices when you use the service more.
With consolidated billing, AWS combines the usage from all accounts to determine which volume pricing tiers to apply, giving you a lower overall price whenever possible.

* Consolidated Billing
  - is strictly an accounting and billing feature.
  - is not a method for controlling accounts, provisioning resources for accounts
  - allows receiving a combined view of charges incurred by all the associated accounts as well as each of the accounts.

* How to set it up?
  1. owner of the PAYING ACCOUNT sends request to LINKED ACCOUNT from the CONSOLIDATED BILLING PAGE.
  2. LINKED owner accepts, becomes part of the consolidated billing
  3. note: CANNOT be initiated from the LINKED ACCOUNT.

* Payer account is billed for all charges of the linked accounts.
* Each linked account is still an independent account in every other way
* Payer account cannot access data belonging to the linked account owners
* However, access to the Payer account users can be granted through Cross Account Access roles
* AWS limits work on the account level only and AWS support is per account only

* Benefits:
  1. One Bill
  2. Easy Tracking
  3. Combined Usage & Volume Discounts (all accounts treated as ONE)
  4. Free Tier (no CB only have access to ONE free usage tier)

  You can use the Consolidated Billing feature to consolidate payment for multiple Amazon Web Services (AWS) accounts or multiple Amazon International Services Pvt. Ltd (AISPL) accounts
  within your organization by designating one of them to be the payer account.
  With Consolidated Billing, you can see a combined view of AWS charges incurred by all accounts, as well as get a cost report for each individual account associated with your payer account.

### [EC2 Reserved Instances](https://aws.amazon.com/ec2/pricing/reserved-instances/)
> Amazon EC2 Reserved Instances (RI) provide a significant discount (up to 75%) compared to On-Demand pricing and provide a capacity reservation when used in a specific Availability Zone.

- All Linked accounts on a consolidated bill can receive the hourly cost benefit of EC2 Reserved Instances purchased by any other account
- Linked accounts receive the cost benefit from other’s Reserved Instances only if instances are launched in the same `Availability Zone` where the Reserved Instances were purchased
- Capacity reservation only applies to the `product platform`, `instance type`, and `Availability Zone` specified in the purchase.


### Programmatic Billing

  When you go to the Billing section and go to preferences you will have the facility to Enable Programmatic access to billing.
  This allows one to receive Alerts, Reports and billing invoices.
  From the below snapshot, you can see that Option B,C and D are valid because they are all available.
  The only option which is not explicitly available is Programmatic access.

  user trying to set up their own billing management system, setup Programmatic billing access.


### [Budgets](https://aws.amazon.com/aws-cost-management/aws-budgets/)

  - Budgets can be used to track AWS costs to see usage-to-date and current estimated charges from AWS
  - Budgets use the cost visualization provided by Cost Explorer to show the status of the budgets and to provide forecasts of your estimated costs.
  - Budgets can be used to create CloudWatch alarms that notify when you go over your budgeted amounts, or when the estimated costs exceed budgets
  Notifications can be sent to an SNS topic and to email addresses associated with your budget notification

### [Cost Allocation Tags](http://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/cost-alloc-tags.html)

  * A tag is a label that you or AWS assigns to an AWS resource. Each tag consists of a key and a value.
  A key can have more than one value. You can use tags to organize your resources, and cost allocation tags to track your AWS costs on a detailed level.
  * After you activate cost allocation tags, AWS uses the cost allocation tags to organize your resource costs on your cost allocation report, to make it easier for you to categorize and track your AWS costs.
    * AWS provides two types of cost allocation tags, an `AWS-generated tag` and `user-defined tags`.
  * AWS defines, creates, and applies the AWS-generated tag for you, and you define, create, and apply user-defined tags.
  * You must activate both types of tags separately before they can appear in Cost Explorer or on a cost allocation report.

### [Consolidated Billing Best Practice](home.html)

- paying account used for PAYING ONLY i.e. accounting & billing
- works best with `RESOURCE TAGGING` as tags are used in detailed billing reports, enabling costs to be analyzed.
- paying accounts should be secured with MFA and a strong password.

### Questions look like...
> Which of the below mentioned AWS services would incur a charge if used?

> A user is trying to save some cost on the AWS services. Which of the below mentioned options will not help him save cost?

### Reference:
https://aws.amazon.com/documentation/account-billing/
