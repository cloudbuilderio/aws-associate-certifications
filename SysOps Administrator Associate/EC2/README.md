# EC2

## Overview

Amazon Elastic Compute Cloud (Amazon EC2) is a web service that provides secure, resizable compute capacity in the cloud. It is designed to make web-scale cloud computing easier for developers. Amazon EC2 reduces the time required to obtain and boot new server instances to minutes, allowing you to quickly scale capacity, both up and down, as your computing requirements change.

## EC2 FAQs

1. Billing
2. Hardware Information
3. Security
4. Elastic IP
5. Availability Zones
6. Enhanced Networking
7. Amazon Elastic Block Store (EBS)
8. Amazon Elastic File System (EFS)
9. Amazon CloudWatch
10. Auto Scaling
11. Elastic Load Balancing

## Key Points

#### [Status Checks](http://docs.aws.amazon.com/)
* **System Status Check (checks PYSHICAL HOST)**
	- loss of Network
	- loss of system power
	- software issues (physical host)
	- hardware issues (physical host)
	- solution:
		* EBS-backed: `stop, then start VM's again` (will start up on a DIFFERENT HOST automatically)
		* Instance-backed: `terminate and launch replacement`

* **Instance Status Check (checks VM)**
	- fails of SYSTEM status check is failed
	- misconfigured network/startup configs
	- run out of memory
	- corrupted filesystem
	- incompatible kernel
	- solution: `rebooting instance`, or modifying your OS.

#### [Instance Reboot](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-reboot.html)
* When you reboot your instance, as per the AWS documentation the following actions occur. An instance reboot is equivalent to an operating system reboot. In most cases, it takes only a few minutes to reboot your instance. When you reboot an instance, it remains on the same physical host, so your instance keeps its public DNS name (IPv4), private IPv4 address, IPv6 address (if applicable), and any data on its instance store volumes.

#### [Stopping & Starting Instances](http://docs.aws.amazon.com/AWSEC2/latest/WindowsGuide/Stop_Start.html#starting-stopping-instances)
* EC2-Classic
	* *[EC2-Classic]* When the instance state becomes stopped, the Elastic IP, Public DNS (IPv4), Private DNS, and Private IPs fields in the details pane are blank to indicate that the old values are no longer associated with the instance.

	* *[EC2-Classic]* When the instance state becomes running, the Public DNS (IPv4), Private DNS, and Private IPs fields in the details pane contain the new values that we assigned to the instance.
* CLI
 	- [stop-instances](http://docs.aws.amazon.com/cli/latest/reference/ec2/stop-instances.html)
		```
		stop-instances
		--instance-ids <value>
		[--dry-run | --no-dry-run]
		[--force | --no-force]
		[--cli-input-json <value>]
		[--generate-cli-skeleton <value>]
		```

		example:
		```bash
		aws ec2 stop-instances --instance-ids i-1234567890abcdef0
		```

		output:
		```json
		{
    	"StoppingInstances": [
        	{
            "InstanceId": "i-1234567890abcdef0",
            "CurrentState": {
                "Code": 64,
                "Name": "stopping"
            },
            "PreviousState": {
                "Code": 16,
                "Name": "running"
		            }
		        }
		    ]
		}
		```

* [start-instances](http://docs.aws.amazon.com/cli/latest/reference/ec2/start-instances.html)
	```
	start-instances
		--instance-ids <value>
		[--additional-info <value>]
		[--dry-run | --no-dry-run]
		[--cli-input-json <value>]
		[--generate-cli-skeleton <value>]
	```

	example:
	```bash
	aws ec2 start-instances --instance-ids i-1234567890abcdef0
	```

	output:
	```json
	{
	    "StartingInstances": [
	        {
	            "InstanceId": "i-1234567890abcdef0",
	            "CurrentState": {
	                "Code": 0,
	                "Name": "pending"
	            },
	            "PreviousState": {
	                "Code": 80,
	                "Name": "stopped"
	            }
	        }
	    ]
	}
	```		


#### [EBS snapshots](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSSnapshots.html)
* backup & recovery: daily EBS snapshots with monthly rotation of snapshots
* [snapshots using CLI](http://docs.aws.amazon.com/cli/latest/reference/ec2/create-snapshot.html)

#### [AMI](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/creating-an-ami-instance-store.html)
* steps for creating an AMI
* credentials required for creating an AMI:
	- **AWS account ID**
	- **X.509 certificate and private key**
	- **AWS login ID to login to the console**
* change ZONE for an AMI (EBS-backed EC2):
	- creating an AMI from the original instance,
	- launching an instance in the new Availability Zone
	- updating the configuration of the new instance
* [copying](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/CopyingAMIs.html)
	> You can copy an Amazon Machine Image (AMI) `within or across an AWS region` using the `AWS Management Console`, the AWS `command line tools` or `SDKs`, or the Amazon `EC2 API`, all of which support `theCopyImage` action.

	> You can copy both Amazon EBS-backed AMIs and instance store-backed AMIs.

	> You can copy AMIs with encrypted snapshots and encrypted AMIs.

	> AWS `does not copy launch permissions, user-defined tags, or Amazon S3 bucket permissions from the source AMI to the new AMI`. After the copy operation is complete, you can _apply launch permissions_, _user-defined tags_, and Amazon _S3 bucket permissions_ to the new AMI.


* [Tags](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Using_Tags.html)
	* Tags enable you to categorize your AWS resources in different ways, for example, by purpose, owner, or environment. As per the AWS documentation below is the restrictions on tags. From the below text it can be seen that the maximum key length is 127 and not 64.

		* Maximum number of tags per resource—50
		* Maximum key length—127 Unicode characters in UTF-8
		* Maximum value length—255 Unicode characters in UTF-8
		* Tag keys and values are case sensitive.
		* Do not use the AWS: prefix in your tag names or values because it is reserved for AWS use. You can't edit or delete tag names or values with this prefix. Tags with this prefix do not count against your tags per resource limit.
		* If your tagging schema will be used across multiple services and resources, remember that other services may have restrictions on allowed characters. Generally allowed characters are: letters, spaces, and numbers representable in UTF-8, plus the following special characters: + - = . _ : / @.

#### [Troubleshooting Instances](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-troubleshoot.html)
- `Host key not found`
	* The user has provided the wrong user name for the OS login

- `Error connecting to your instance: Connection timed out`
	* check ROUTE TABLE has route to IGW
	* check SG is not allowing INBOUND traffic from Public IP
	* check NACL allows INBOUND
	* private key mismatch
	* login username wrong

- `User key not recognized by server`
	* private key not converted to required format

- `Host key not found`
	* The user has provided the wrong user name for the OS login

- `InsufficientInstanceCapacity`
	* AWS does not currently have enough available capacity to service your request. There is a limit for number of instances of instance type that can be launched within a region.
- `InstanceLimitExceeded`
	* Concurrent running instance limit, **default is 20**, has been reached.

### Questions look like...
> AWS-managed solutions as much as possible and automate the rest with the AWS CLI and scripts.
Which task would be best accomplished with a script?

### References:
* [https://aws.amazon.com/ec2/faqs/]
