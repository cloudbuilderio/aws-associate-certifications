# AWS Shared Responsibility Model

## Overview

## Key Points
* Compliance (audits) preparation:
  * Gather evidence of your IT Operational controls
  * Request and obtain third party audited AWS compliance reports and certifications,
  * Request and obtain approval from AWS to perform relevant network scans and in-depth penetration tests of your system's Instances and endpoints.


### Questions look like...
> You are running a web-application on AWS consisting of the following components
an Elastic Load Balancer (ELB) an Auto-Scaling Group of EC2 instances running Linux/PHP/Apache,
and Relational Database Service (RDS) MySQL. Which security measures fall into AWS’s responsibility?

> What are the 3 best practises for you to prepare for an audit?

### Reference:
* [https://aws.amazon.com/blogs/security/tag/shared-responsibility-model/]
