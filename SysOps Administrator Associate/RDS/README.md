# RDS

## Overview

## Key Points
#### [RDS Automated Backups](http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_WorkingWithAutomatedBackups.html)
  * Automated backups automatically back up your DB instance during a specific, user-definable backup window. Amazon RDS keeps these backups for a limited period that you can specify. You can later recover your database to any point in time during this backup retention period. And all of these backups get stored to S3 by default. The backup retention period is present as a setting when you setup the RDS in AWS.


#### [Multi-Availability Zone Deployments](http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Concepts.MultiAZ.html)
  * Amazon RDS Multi-AZ deployments provide enhanced availability and durability for Database (DB) Instances, making them a natural fit for production database workloads.
  * When you provision a Multi-AZ DB Instance, Amazon `RDS automatically creates a primary DB Instance and synchronously replicates the data to a standby instance in a different Availability Zone (AZ)`.
  * Each AZ runs on its own physically distinct, independent infrastructure, and is engineered to be highly reliable.
  In case of an infrastructure failure (for example, instance hardware failure, storage failure, or network disruption), `Amazon RDS performs an automatic failover to the standby, so that you can resume database operations as soon as the failover is complete`.
    * **If the `Primary` fails** = The canonical name record (CNAME) is changed from primary to standby (i.e. DNS is updated to point to the standby DB)
    * **for `High Latency`** = For production workloads, we recommend you use Provisioned IOPS and DB instance classes (m1.large and larger) that are optimized for Provisioned IOPS for fast, consistent performance.
      * Also if backups are scheduled during working hours , then I/O can be suspended and increase the latency of the DB, hence `it is better to schedule outside of office hours`.
    * **Failover Conditions**
      > DB Instance failover is fully automatic and requires no administrative intervention. Amazon RDS monitors the health of your primary and standbys, and initiates a failover automatically in response to a variety of failure conditions.

      * Amazon RDS detects and automatically recovers from the most common failure scenarios for Multi-AZ deployments so that you can resume database operations as quickly as possible without administrative intervention.
      * Amazon RDS automatically performs a failover in the event of any of the following:
        - Loss of availability in primary Availability Zone
        - Loss of network connectivity to primary
        - Compute unit failure on primary
        - Storage failure on primary

      > Note: When operations such as DB Instance scaling or system upgrades like OS patching are initiated for Multi-AZ deployments, for enhanced availability, they are applied first on the standby prior to an automatic failover (see the Aurora documentation for details on update behavior). As a result, your availability impact is limited only to the time required for automatic failover to complete. Note that Amazon RDS Multi-AZ deployments do not failover automatically in response to database operations such as long running queries, deadlocks or database corruption errors.

#### Read Replica
* RDS uses the
  - **PostgreSQL**,
  - **MySQL**, and
  - **MariaDB** DB engines’
  built-in replication functionality to create a special type of DB instance called a `Read Replica` from a `source DB instance`.
* Up to **5 RR max** from the one DB source.
* Load on the source DB instance can be reduced by routing read queries from applications to the Read Replica.
* Read Replicas allow elastic scaling beyond the capacity constraints of a single DB instance for read-heavy database workloads
* Failover: if a Multi-AZ DB switches to Secondary, all RR will switch to that DB too.
* If the source DB instance is deleted without deleting the replicas, each replica is `promoted to a stand-alone, single-AZ DB instance`.
* RDS **does not** support circular replication = the RR can't be a source for the existing DB instance.
* Cross-Region replication
  - MySQL, PostgreSQL, MariaDB = can do cross-region
  - good for DR (reduces RTO and RPO)
  - scale READ closer to users
  - migrate to DC in different region


#### [Maintenance Windows](maintenance.html)

>The Amazon RDS maintenance window is your opportunity to control when DB instance modifications (such as scaling DB instance class) and software patching occur, in the event they are requested or required. If a maintenance event is scheduled for a given week, it will be initiated and completed at some point during the maintenance
  window you identify. Maintenance windows are 30 minutes in duration.

  * The only maintenance events that require Amazon RDS to take your DB instance offline are scale compute operations (which generally take only a few minutes from start-to-finish) or required software patching. Required patching is automatically scheduled only for patches that are security and durability related. Such patching occurs infrequently (typically once every few months) and should seldom require more than a fraction of your maintenance window. *If you do not specify a preferred weekly maintenance window when creating your DB instance, a 30 minute default value is assigned*. If you wish to modify when maintenance is performed on your behalf, you can do so by modifying your DB instance in the `AWS Management Console`, the `ModifyDBInstance API` or the `modify-db-instance` command. Each of your DB instances can have different preferred maintenance windows, if you so choose.
  * Running your DB instance as a Multi-AZ deployment can further reduce the impact of a maintenance event.


#### [Notifications](http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_Events.html)
Amazon RDS uses the Amazon Simple Notification Service (Amazon SNS) to provide notification when an Amazon RDS event occurs. These notifications can be in any notification form supported by Amazon SNS for an AWS region, such as an email, a text message, or a call to an HTTP endpoint.

Amazon RDS groups these events into categories that you can subscribe to so that you can be notified when an event in that category occurs. You can subscribe to an event category for a
  - DB instance
  - DB cluster
  - DB snapshot
  - DB cluster snapshot
  - DB security group, or for a
  - DB parameter group.


### Questions look like...
> Which Amazon RDS feature will allow you to reliably restore your database to within 5 minutes of when the mistake was made?
