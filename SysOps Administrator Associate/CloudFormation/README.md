# CloudFormation

## Overview
AWS CloudFormation gives developers and systems administrators an easy way to create and manage a collection of related AWS resources, provisioning and updating them in an orderly and predictable fashion.

You can use AWS CloudFormation’s sample `templates` or create your own templates to describe the AWS resources, and any associated dependencies or runtime parameters, required to run your application. You don’t need to figure out the order for provisioning AWS services or the subtleties of making those dependencies work. CloudFormation takes care of this for you. After the AWS resources are deployed, you can modify and update them in a controlled and predictable way, in effect applying version control to your AWS infrastructure the same way you do with your software

## Key Points

#### CloudFormation is...

* A [TEMPLATE](https://aws.amazon.com/cloudformation/aws-cloudformation-templates/)
  - architectural diagram
  - JSON or YAML format, text-based, describing resources need to deploy and run your application.
  - template components
    * list of resources/values
    * file format version number (optional)
    * list of template parameters (optional)
    * list of output values (optional) e.g. public IP address
    * list of data tables for looking up static configuration (optional) e.g. AMI names per AZ

* A STACK
  - result of a deployed TEMPLATE.

* used to setup resources consistently, repeatedly across multiple regions.
* version control of resource updates, deletes, modifications
* does not charge the user for its service but only charges for the AWS resources created with it (running)
* CF will always ROLLBACK on failure, deleting everything it created.
* templates are FREE, you only pay for the RESOURCES CF provisions.
* supports Puppet/Chef
* provides set of application bootstrapping scripts to install software etc
* allows delete policies where you can specify resources to be retained, snapshots created etc before a stack deletes itself.
* works with a wide variety of AWS services, such as EC2, EBS, VPC, IAM, S3, RDS, ELB, etc
* use _**WaitCondition**_ if you need resource A to wait for resource B to be launched first
   You can use a wait condition for situations like the following:
   - To coordinate stack resource creation with configuration actions that are external to the stack creation
   - To track the status of a configuration process

#### Access control

##### IAM
* can be applied to CF to access control for users whether they can view stack templates, or create, delete stacks.
* CF validates the template to check what resources it will create

##### Service Role
* IAM Role which lets CF make resource calls on the users behalf
* default - CF uses temporary sessions generated from the users credentials for stack operations.
* for a service role, CF users the ROLES credentials.
* if SR is specified - CF will always use that role for all operations on that stack.

#### Template Resource Attributes
* **CreationPolicy** Attribute
  is invoked during the associated resource creation
  can be associated with a resource to prevent its status from reaching create complete until AWS CloudFormation receives a specified number of success signals or the timeout period is exceeded
  helps to wait on resource configuration actions before stack creation proceeds for e.g. software installation on an EC2 instance
* **DeletionPolicy** Attribute
  preserve or (in some cases) backup a resource when its stack is deleted
  By default, if a resource has no DeletionPolicy attribute, AWS CloudFormation deletes the resource
* To keep a resource when its stack is deleted,
  - specify **Retain** for that resource, to prevent deletion
  - specify **Snapshot** to create a snapshot before deleting the resource, if the snapshot capability is supported for e.g RDS, EC2 volume etc.
* **DependsOn** Attribute
  helps specify that the creation of a specific resource follows another
  resource is created only after the creation of the resource specified in the DependsOn attribute
* **Metadata** Attribute
  enables association of structured data with a resource
* **UpdatePolicy** Attribute
  defines AWS CloudFormation handles updates to the AWS::AutoScaling::AutoScalingGroup resource

### Questions look like...
>Your team is excited about the use of AWS because now they have access to "programmable Infrastructure”.
You have been asked to manage your AWS infrastructure In a manner similar to the way you might manage application code
You want to be able to deploy exact copies of different versions of your infrastructure, stage changes into different environments,
revert back to previous versions, and identify what versions are running at any particular time (development test QA . production).
Which approach addresses this requirement?

### Exam Tip:
* when you see "automated deployment", "programmable infrastructure", think CloudFormation.

### Reference:
* [https://aws.amazon.com/cloudformation/]
