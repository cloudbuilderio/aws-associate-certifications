# IAM

## Overview
AWS Identity and Access Management (IAM) enables you to securely control access to AWS services and resources for your users. Using IAM, you can create and manage AWS users and groups, and use permissions to allow and deny their access to AWS resources. 

IAM is a feature of your AWS account offered at no additional charge. You will be charged only for use of other AWS services by your users.

## Key Points

* IAM is used to control
  * **Identity** – who can use your AWS resources (authentication)
  * **Access** – what resources they can use and in what ways (authorization)

#### IAM Features:

* Shared Access to your AWS account
* Granular Permissions
* Secure access to AWS resources for applications that run on EC2
* Identity Federation
* Id info for assurance (e.g. cloudtrail capture logs for auditing)
* PCI DSS Compliance
* Integrates with many AWS services
* Free
* AWS Security Token Service (STS)


#### [IAM best practices](http://docs.aws.amazon.com/IAM/latest/UserGuide/best-practices.html):
* The best practice for IAM is to create roles which has specific access to an AWS service and then give the user permission to the AWS service via the role.

#### IAM Root User
* best practice: do not use or share root account once created, create a separate user with admin privileges.

#### [IAM users](http://docs.aws.amazon.com/IAM/latest/UserGuide/introduction.html)
* best practice: create individual users, least privilege.
* user credentials = password (used for = management console), access key/secret access key (used for = API, CLI, SDK)
* (default) users = no permissions, not authorized to perform ANY AWS actions
* 1 x user = 1 x aws account
* It is not possible in any way to have the same ID and multiple passwords for different IAM users.

#### IAM Groups
* best practice: use groups to assign permissions to IAM users
* IAM user can belong to MAX=10 groups
* can't be nested, only holds IAM Users
* no default group
* deleting groups = you need to detach users, managed & in-line policies before deleting group. note: management console takes care of detach-before-delete.

#### IAM Roles
* best practice: enable MFA on root account and privileged users
* MFA (hardware/virtual device) can be configured using:
  - STS
  - SMS
* if MFA is lost, or stops working you need to contact AWS to regain access to AWS console. 

#### Examples

[Access Policy Examples](http://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_examples.html#iam-policy-example-ec2-two-conditions)
  - "Access IAM console from INSIDE the organization and not outside"?
    * Create an IAM policy with a condition which DENIES access when the IP address range is NOT from the organization

    ```json
    {
      "Version": "2012-10-17",
      "Statement": {
        "Effect": "Deny",
        "Action": "*",
        "Resource": "*",
        "Condition": {"NotIpAddress": {"AWS:SourceIp": [
          "192.0.2.0/24",
          "203.0.113.0/24"
        ]}}
      }
    }
    ```
  - Admin access or "everything/everything" permissions

    ```json
    {
    "Version": "2012-10-17",
    "Statement": [
    {
      "Effect": "Allow",
      "Action": "*",
      "Resource": "*"
    }]}
    ```

* [Reading Policy](http://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_understand-policy-summary.html)
  * `“Action”: “ec2:Describe*”` = READ
  * List, Get, Describe = READ
  ```
  “Action”: [
            “cloudwatch:ListMetrics”,
            “cloudwatch:GetMetricStatistics”,
            “cloudwatch:Describe*”
            ],
  ```
  * Format = "Action": "<resource>:<entitlements>", where resource=cloudwatch, ec2, elasticloadbalancing

### Questions look like...
> An organization has created 5 IAM users. The organization wants to give them the same login ID but different passwords. How can the organization achieve this?

> organization has 50 IAM users. new policy introduced, how do we implement it so we dont need to do it individually?


### Reference:
