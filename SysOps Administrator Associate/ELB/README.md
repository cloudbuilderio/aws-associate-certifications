# ELB

## Overview

## Key Points

#### Scaling ELB (see Auto Scaling below)
	- ELB allocated and configured with default Capacity
	- ELB Controller stores config, monitors LB capacity to handle client requests
	- can scale UP (increase resources) or scale OUT (increase instances)
	- AWS handles the ELB capacity scaling itself (this is different to Auto Scaling)

#### Pre-Warming ELB
	- ELB works best for gradual traffic increase
	- if known spikes coming, you want to PRE-WARM your ELB volumes
	- contact AWS to help with this, you will need:
		- start and end dates of spike
		- expected request rate/s
		- total size of request/response

#### DNS Resolution
	- DNS TTL 60 seconds
	- ELB updates the DNS records of load balancer with new resources IPs are updated & resolvable in DNS.
	- recommended clients re-lookup DNS every 60s to take advantage of any new scaled capacity.

#### Load Balancer Types
	* Internet LB = internet-facing LB routes to back-end EC2
	* Internal LB = routes traffic to EC2 in private subnets.

#### Availability Zones / Subnets

	- ONE subnet per AZ can be attached to ONE ELB = attaching new ones REPLACE old one.
	- Subnet must; /27 and has at least 8 free IP addys used by ELB to connect to back-end.
	- for HA - recommended attach ONE subnet PER AZ for at least TWO AZ's - even if single subnet.

#### Security & NACL
	- should allow inbound traffic
		* on the LB listening port
		* from client for an internet ELB
		* VPC CIDR for an internal ELB
		* All EC2 should allow incoming traffic from ELB .

#### Health Checks

	- Load balancer performs health checks on all registered instances, whether the instance is in a healthy state or an unhealthy state.
	- Load balancer performs health checks to discover the availability of the EC2 instances, the load balancer periodically sends pings, attempts connections, or sends request to health check the EC2 instances.
	- Health check is InService for status of healthy instances and OutOfService for unhealthy ones
	- Load balancer sends a request to each registered instance at the `Ping Protocol`, `Ping Port` and `Ping Path` every HealthCheck Interval seconds. It waits for the instance to respond within the `Response Timeout period`. If the health checks exceed the Unhealthy Threshold for consecutive failed responses, the load balancer takes the instance out of service. When the health checks exceed the Healthy Threshold for consecutive successful responses, the load balancer puts the instance back in service.
	- Load balancer only sends requests to the healthy EC2 instances and stops routing requests to the unhealthy instances


#### [Security Policy](http://docs.aws.amazon.com/elasticloadbalancing/latest/classic/elb-security-policy-options.html)
A security policy determines which ciphers and protocols are supported during SSL negotiations between a client and a load balancer.
You can configure your Classic Load Balancers to use either predefined or custom security policies.

##### predefined policies
The names of the most recent predefined security policies includes `version information` based on the `year` and `month` that they were released. For example, the default predefined security policy is `ELBSecurityPolicy-2016-08`.

##### custom policies
You can create a custom negotiation configuration with the ciphers and protocols that you need. For example, some security compliance standards (such as PCI and SOC) might require a specific set of protocols and ciphers to ensure that the security standards are met. In such cases, you can create a custom security policy to meet those standards.

##### SSL Protocols
The following versions of the SSL protocol are supported: TLS 1.2, TLS 1.1, TLS 1.0, SSL 3.0

##### Deprecated SSL Protocols:
SSL 2.0

##### Server Order Preference
Elastic Load Balancing supports the Server Order Preference option for negotiating connections between a client and a load balancer. During the SSL connection negotiation process, the client and the load balancer present a list of ciphers and protocols that they each support, in order of preference. By default, the first cipher on the client's list that matches any one of the load balancer's ciphers is selected for the SSL connection. If the load balancer is configured to support Server Order Preference, then the load balancer selects the first cipher in its list that is in the client's list of ciphers. This ensures that the load balancer determines which cipher is used for SSL connection. If you do not enable Server Order Preference, the order of ciphers presented by the client is used to negotiate connections between the client and the load balancer.

##### SSL Ciphers

#### [Sticky Sessions](http://docs.aws.amazon.com/elasticloadbalancing/latest/classic/elb-sticky-sessions.html)

##### Duration-Based:
The load balancer uses a special cookie to track the instance for each request to each listener. When the load balancer receives a request, it first checks to see if this cookie is present in the request. If so, the request is sent to the instance specified in the cookie. If there is no cookie, the load balancer chooses an instance based on the existing load balancing algorithm. A cookie is inserted into the response for binding subsequent requests from the same user to that instance. The stickiness policy configuration defines a cookie expiration, which establishes the duration of validity for each cookie. `After a cookie expires, the session is no longer sticky.`

If an instance fails or becomes unhealthy, the load balancer stops routing requests to that instance, and chooses a new healthy instance based on the existing load balancing algorithm. The request is routed to the new instance as if there is no cookie and the session is no longer sticky.

##### Application-Controlled:
The load balancer uses a special cookie to associate the session with the instance that handled the initial request, but follows the lifetime of the application cookie specified in the policy configuration. The load balancer only inserts a new stickiness cookie if the application response includes a new application cookie. The load balancer stickiness cookie does not update with each request. `If the application cookie is explicitly removed or expires, the session stops being sticky until a new application cookie is issued.`

If an instance fails or becomes unhealthy, the load balancer stops routing requests to that instance, and chooses a new healthy instance based on the existing load balancing algorithm. The load balancer treats the session as now "stuck" to the new healthy instance, and continues routing requests to that instance even if the failed instance comes back.

If a client switches to a listener with a different backend port, stickiness is lost.

##### Deleting Load Balancer:
Deleting a load balancer does not affect the instances registered with the load balancer and they would continue to run

#### Cross-Zone

* By default, the load balancer distributes incoming requests evenly across its enabled Availability Zones for _e.g. If AZ-a has 5 instances and AZ-b has 2 instances, the load will still be `distributed 50% across each of the AZs`_

* CZ will let you distribute load across ALL AZ's regardless of how many instances in each AZ. So the application can be balanced across the total sum of back-end instances regardless of AZ.

* still recommended to maintain approx even #'s of instances in each AZ for high fault tolerance.


#### [Connection Draining](http://docs.aws.amazon.com/elasticloadbalancing/latest/classic/config-conn-drain.html)
* helps the user to stop sending new requests traffic from the load balancer to the EC2 instance when the instance is being deregistered while continuing in-flight requests.

* MAX timeout = 3600s = 1 hour

#### [Traffic Handling](http://docs.aws.amazon.com/elasticloadbalancing/latest/classic/elb-listener-config.html)
* SSL Listener:
	* Listeners support HTTP, HTTPS, SSL, TCP protocols
	* (TERMINATE ON ELB) A X.509 certificate is required for HTTPS or SSL connections and load balancer uses the certificate to terminate the connection and then decrypt requests from clients before sending them to the back-end instances.
	* ELB will not modify headers

* (TERMINATE ON BACKEND) use TCP for connections from the client to the load balancer, use the SSL protocol for connections from the load balancer to the back-end application, and deploy certificates on the back-end instances handling requests.

* ELB HTTPS listener **does not support Client-Side SSL certificates**.

* X-Forwarded Headers - help back-end server track client connection info (cant see it if ELB in between)
	- **X-Forwarded-For** - client IP
	- **X-Forwarded-Proto** - protocol, http/s
	- **X-Forwarded-Port** - port used by ELB connect to client

* Proxy Protocol Support
	- an Internet Protocol for carrying info from the client side to the back-end side
	- ELB uses PP v1, human readable header w/ info like IP address, destination IP address, port numbers
	- **if you enable Proxy Protocol Support and your ELB is already BEHIND a Proxy, you will get the HEADER TWICE**

#### [Access Logs](http://docs.aws.amazon.com/elasticloadbalancing/latest/application/load-balancer-access-logs.html)
* log file format
	- `bucket[/prefix]/AWSLogs/AWS-account-id/elasticloadbalancing/region/yyyy/mm/dd/AWS-account-id_elasticloadbalancing_region_load-balancer-id_end-time_ip-address_random-string.log.gz`
	- contains:
	> `bucket`
		The name of the S3 bucket.

		>`prefix`
		The prefix (logical hierarchy) in the bucket. If you don't specify a prefix, the logs are placed at the root level of the bucket.

		>`aws-account-id`
		The AWS account ID of the owner.

		>`region`
		The region for your load balancer and S3 bucket.

		>`yyyy/mm/dd`
		The date that the log was delivered.

		>`load-balancer-id`
		The resource ID of the load balancer. If the resource ID contains any forward slashes (/), they are replaced with periods (.).

		>`end-time`
		The date and time that the logging interval ended. For example, an end time of 20140215T2340Z contains entries for requests made between 23:35 and 23:40.

		>`ip-address`
		The IP address of the load balancer node that handled the request. For an internal load balancer, this is a private IP address.

		>`random-string`
		A system-generated random string.

## ELB & Auto-Scaling

- makes it easy to route traffic across a dynamically changing fleet of EC2 instances
- load balancer acts as a single point of contact for all incoming traffic to the instances in an Auto Scaling group.

#### Attach/Detach ELB with ASG
- Kick off ELB & Auto-Scaling by ATTACHING one or more ELBs to an `Auto Scaling Group (ASG)`.
- once attached the ELB automatically registers the EC2 backend servers by their IPs, and routes requests from the ELB front-end to these instances
- **Detach**
	* ELB enters `Removing` state while deregistering instances in the group
	* if `connection draining` enabled, waits for in-flight requests to complete before deregistering those instances.
	* instances remain `RUNNING` after deregistering.
	* ASG auto add instances to ELB as they're launched, but this can be `Suspended`. If you add during Suspended, these instances aren't added to ELB, and when the Suspension is lifted, you have to add these `MANUALLY`.

#### HA
- AS can span Multi-AZ, IN same Region.
- when an AZ becomes `unhealthy`, AS will launch in an unaffected ZONE.
- when unhealthy AZ's come back, AS will redistribute across ALL AZ's again.
- **recommended:** use AS & ELB **spanning ASG across multiple AZ's in a region, and using ELB to distribute traffic to THOSE AZs**

### Questions look like...
> Which of the below mentioned statements helps the user understand ELB traffic handling with respect to the SSL listener?

### References:
