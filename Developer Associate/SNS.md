# Amazon Simple Notification Service (SNS)

is a web service that enables you to send messages to endpoints that subscribe to receiving it.  Publishers and subscribers, otherwise known as producers and consumers are the primary terminologies used when describing the functioning of Amazon SNS.  Publishers produce and send messages to a ‘topic’ asynchronously and subscribers subscribe to those topics in order to consume those messages.  For the AWS Certified Solutions Architect Associate Exam, you need to understand the common uses of Amazon SNS and under what scenarios you would recommend using it.  In addition, you must understand some core features as well as limitations of the service.

Subscribers can be web servers, email address, SQS queues or Lambda functions and they can receive messages using the following supported protocols:

### Amazon SQS.
Note that users should create the SQS queue prior to subscribing it to a Topic. In addition, Amazon SNS does not currently support forwarding messages to Amazon SQS FIFO queues. Only standard queues are supported
HTTP/HTTPS
### Email and Email-JSON.
“Email-JSON” sends notifications as a JSON object, and is used by applications to programmatically process emails. The ”Email” transport is meant for end-users and notifications are regular, text-based messages which are easily readable. It should be noted that emails can sometimes end up in junk folders and as such users can add “no-reply@sns.amazonaws.com” to their contact lists and check their junk/spam folders for messages from Amazon SNS.

SMS
Lambda

### Topics
Topics are a means to enable the distribution of messages from publishers to subscribers and involve defining policies that determine which publishers and subscribers can communicate with the topic.  Publishers will send messages to topics rather than directly to subscribers. Amazon SNS then deliver those messages to subscribers of the topic. Topics have unique names which enable publishers to identify them and subscribers to register for notifications. Note that all subscribers to a particular topic receive the same messages to that topic.  One topic can support deliveries to multiple endpoint types and so you do not need to worry about the format required for the end recipients to read the message.

#### Common Amazon SNS Scenarios
Amazon SNS can be used in a variety of scenarios including:
* Monitoring applications and services
* Providing time sensitive information
* Delivering messages at regular intervals and on the basis of certain triggers
* Event updates notification services, for example, autoscaling notifications

### ‘Fanout’ Scenario
This is when an Amazon SNS Message is sent to a topic and then replicated and pushed out to multiple subscribers including Amazon SQS queues, HTTP/HTTPS endpoints and email addresses. This scenario is particularly useful where parallel asynchronous processing is required and you need to send identical notifications to different endpoints. For example, in an order placement scenario, you can replicate the order placement notification to be pushed out to multiple subscribers such as:
End customer’s email address for order placement notification
Accounts department’s accounting software to initiate the dispatch and invoicing process
Data warehouse application for analytics

### Application and Systems Alerts
Alert notification messages can be pushed out to subscribers who need to receive immediate notifications of any events that occur with their application or systems based on certain predefined thresholds. An example includes being notified when your Windows EC2 instances are running out disk space.

### Push Email and Text Messaging
You can use Amazon SNS to transmit messages in the form or push email or text to individuals and groups via email and SMS respectively. Amazon SNS can be configured to push a variety of information to subscribers such as news headlines, weather reports or excerpts of information where the subscriber can then learn more by clicking on a link in the notification message.

### Mobile Push Notifications
Amazon SNS is often used to send out mobile push notification messages directly to mobile apps. Often used to notify you of updates to mobile apps, the notification messages will then prompt you to download the updates and install for your mobile applications.

### Amazon SNS vs. Amazon SQS
Unlike other Amazon services such as SQS which utilise a polling technology where periodic checks or ‘polls’ are made for updates, Amazon SNS allows applications to send time-critical messages to subscribers using a ‘push’ mechanism. Amazon SNS is more suited to certain scenarios where SQS may not necessarily be the best service to use. As part of the exam, it’s important you understand which service to use under what scenario and business requirement.

### Additional Exam Tips
Amazon SNS follows the “publish-subscribe” concept, and notifications being delivered to clients using a “push” mechanism that eliminates the need to periodically check or “poll” for new information and updates.  Uses of using Amazon SNS include:
* Instantaneous push-based delivery (no polling)
* API integration with applications
* Messages can be delivered over a number of transport protocols
* Management console to offer simple point and click interface

To setup Amazon SNS, you need to create topics which are essential ‘access points’ to help identify specific subjects or event types. You can then publish messages to those topics and allow endpoints to subscribe to it for notifications.  You can set policies such as limiting who can publish messages and who can subscribe to notifications as well as determine which protocols will be supported.  Publishers will publish messages to topics which in turn triggers Amazon SNS to deliver the message to all applicable subscribers

### Features and Functionality
The format of an Amazon SNS topics are as follows:
- Topic names are limited to 256 characters
  * These characters include alphanumeric characters, hyphens (-) and underscores
- Topic names must be unique within an AWS account
- Topic names can be reused after deletion. Topic names will be available for reuse approximately 30-60 seconds after the previous topic with the same name has been deleted.
- Amazon SNS will assign a unique ARN (Amazon Resource Name) to the topic, which will include the service name (SNS), region, AWS ID of the user and the topic name when you create new topic names
- Topics can support subscriptions and notification deliveries over multiple modes of transport

### Reliability
Messages are stored redundantly across multiple servers and data centres to ensure reliability
All notifications contain a single published message
Messages will be delivered exactly once most of the time; however occasional duplication can be experienced at the subscriber’s end
Amazon SNS will attempt to deliver messages in the order it was published, but this is not 100% guaranteed
Messages cannot be deleted once published to a topic. There is no recall facility
Amazon SNS guarantees that each message is delivered to Amazon SQS at least once. However, for no-SQS endpoints, the notification may not successfully reach an HTTP or Email endpoint. For HTTP endpoints, an SNS Delivery Policy can be used to control the retry pattern (linear, geometric, exponential backoff), maximum and minimum retry delays, and other parameters. for critical messages to be successfully processed, developers should have notifications delivered to an SQS queue (in addition to notifications over other transports)

### Limits
* **You are allowed 10 million subscriptions per topic and up to 100,000 topics per account**
* SNS messages can contain up to **256 KB** of text data which can include `XML, JSON and unformatted text`
* Each 64KB chunk data is billed as 1 request. A 256KB payload is therefore billed as 4 requests
* For SMS subscriptions, only 140 characters will be included in the payload of the message delivered. This will include the DisplayName of the topic and as many characters of the published message as can be accommodated
