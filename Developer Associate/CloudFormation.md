# Amazon CloudFormation

you can design and deploy a collection of related AWS resources that can then be customised into a `template` for **repeat deployment**. You can then provision and/or update your resources in an orderly fashion with predictable outcomes.

After the AWS resources are deployed, you can modify and update them in a controlled and predictable way, in effect `applying version control to your AWS infrastructure the same way you do with your software`. You can also visualise your templates as diagrams and edit them using a drag-and-drop interface with the AWS CloudFormation Designer.

### Templates
You can create templates that describe AWS resources (for example, with EC2 instance, you can specify instance type, the AMI ID, block device mappings, and Amazon EC2 key pair names), its associated dependencies and any configuration items that need to be defined for successful deployments. Furthermore, Amazon will then deploy your resources taking care to ensure that these services are deployed in the correct order and provision all back-end infrastructure required.

#### Key Points:
* You can add input parameters whose values are specified when you create an AWS CloudFormation stack at run time.
As and when you need to upgrade your templates, you can modify them with versioning capabilities to ensure that any updates/upgrades are controlled. In addition, you can visualise your templates as diagrams and even edit them using a drag and drop interface with the AWS `CloudFormation Designer`.

* You can deploy and update a template and its associated collection of resources (called a stack) by using the AWS Management Console, AWS Command Line Interface, or APIs. CloudFormation. CloudFormation is available free of cost, but you still have to pay for the underlying resources you deploy from your template.

* CloudFormation enables you to replicate your infrastructure in multiple regions quickly without having to manually build out your infrastructure from scratch in every region. You can reuse your templates to set up your resources consistently and repeatedly.

### Version Control
CloudFormation templates are text files, and you can use a version control system with your templates so that you know exactly what changes were made and who made them. This means you don’t have to manually remember which resources were changed whenever you update your stack.

### Stack
In CloudFormation you deploy and manage related resources as a single unit called a stack. The resource that is used to build your stack is already described in your template.
_For example, your template can describe Auto Scaling group, Instance types, EIPs, Elastic Load Balancer, and an Amazon Relational Database Service (Amazon RDS) database instance. To create these resources in your template, you create a stack and submit your template. Amazon CloudFormation will then provision your resources as described in your template._

### Changed Sets
Sometimes you might need to make changes to the running resources in a stack. You can generate a change set, which is a summary of your proposed changes. Change sets allow you to see how your changes might impact your running resources, especially for critical resources, before implementing them.
AWS CloudFormation makes the changes to your stack only when you decide to roll out a change set, and thus you get to decide whether to proceed with your proposed changes or explore other changes by creating another change set.

#### Key Points:
* Creating a Change Set – You need to submit the changes that you want to make by providing a modified template or any new parameter values or both. AWS CloudFormation will generate a change set by comparing your stack with the changes you have proposed

* Viewing the Change Set – You can view the proposed changes before executing them by using the AWS CloudFormation console, AWS CLI, or AWS
CloudFormation API.

* Execute the Change Set – To make the changes described in a change set to your stack, execute the change set. Note that upon execution of a change set, AWS CloudFormation will delete all change sets that are associated with the stack since they are now no longer valid for the updated stack. If the update fails, you will need to recreate a change set

* Deleting a Change Set – Deleting a change set removes it from the list of change sets for the stack. This way, you prevent another user from accidentally executing a change set that shouldn’t be applied.

**Important**
Updates can cause interruptions. Depending on the resource and properties that you are updating, an update might interrupt or even replace an existing resource  Also Note – Change sets don’t indicate whether your stack update will be successful.

* Deleting a Stack
When you delete a stack, you specify the stack to delete, and AWS CloudFormation deletes the stack. You can delete stacks by using the AWS CloudFormation console, API, or AWS CLI.

**When you delete a stack the default action includes the deletion of resources that was launched with that stack**. If you need to keep a resource even after deleting a stack, you can `define a DeletionPolicy attribute for each resource that you retain`.
_For example, you can retain a nested stack, S3 bucket, or EC2 instance so that you can continue to use or modify those resources after you delete their stacks._

```json
JSON Script
{
“AWSTemplateFormatVersion” : “2010-09-09”,
“Resources” : {
“myS3Bucket” : {
“Type” : “AWS::S3::Bucket”,
“DeletionPolicy” : “Retain”
}
}
}
```

#### Note
If you want to modify resources outside of AWS CloudFormation, use a retain policy and then delete the stack. Otherwise, your resources might get out of sync with your AWS CloudFormation template and cause stack errors.

### Permissions
Although you can create templates which can then be used to create a stack of your underlying resources, you still need to ensure that you have the appropriate permissions to deploy those resources.
You can use IAM to configure necessary permissions for your team so that only authorised personnel can create and deploy resources and therefore use CloudFormation to automate the task
Note that CloudFormation templates are written in JSON or YAML format. You can use the CloudFormation Designer or write a template in a text editor.

```json
JSON Format
{
“AWSTemplateFormatVersion” : “2016-05-06”,
“Description” : “A simple EC2 instance”,
“Resources” : {
“MyEC2Instance” : {
“Type” : “AWS::EC2::Instance”,
“Properties” : {
“ImageId” : “ami-6g878987”,
“InstanceType” : “t1.micro”
}
}
}
}
```

YAML Syntax

```yaml
AWSTemplateFormatVersion: ‘2016-05-06’
Description: A simple EC2 instance
Resources:
MyEC2Instance:
Type: AWS::EC2::Instance
Properties:
ImageId: ami-6g878987
InstanceType: t1.micro
```

### Exam Tips

#### RollBacks
Rollbacks are **enabled by default** so that if your deployment fails, CloudFormation will attempt a rollback.
A stack may fail in the rollback process with the UPDATE_ROLLBACK_FAILED state. AWS CloudFormation cannot roll back all changes during an update. For example, you might have a stack that begins to roll back to an old database instance that was deleted outside of AWS CloudFormation.
The error must be fixed which caused the update rollback to fail before you can continue to roll back your stack. You can continue to roll back the update without any changes, for example when a stack operation times out.
Key Note: If you use nested stacks, rolling back the parent stack will attempt to roll back all the child stacks as well.

#### Output
When creating your templates, you can specify Outputs section to declare output values which you can programmatically use to import into other stacks (to create cross-stack references) or return in response to provide required pieces of information. For example, you can output the S3 bucket name for a stack to make the bucket easier to find. Note that the function used to get output value is “Fn::GetAtt”

#### 'Resource' Section of Template is MANDATORY
if it's missing in a question, look for "this template is invalid" or "this template will throw an error" as an answer. 
