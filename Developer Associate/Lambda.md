# AWS Lambda

is a cutting-edge service from Amazon that enables you to deploy Serverless computing capabilities by enabling you to run your code in response to events and automatically manage the underlying compute resources for you. You can build complete applications by utilizing AWS services and running code in the response of event triggers such as such as HTTP requests via Amazon API Gateway, modifications to objects in Amazon S3 buckets and table updates in Amazon DynamoDB.  Lambda runs your code in highly available AWS infrastructure that scales on demand and ensures necessary capacity provisioning, security patch deployment, monitoring and logging.  

With Lambda, you do not have to provision any infrastructure manually or by running any scripts.  Amazon configures all necessary infrastructure components requirement to run your code, which includes compute, storage and memory configurations.  The only requirement is that you code is in Node.js, Java, C# or Python languages.   To make Lambda truly a `Serverless` experience, **you cannot log in to compute instances, or customize the operating system or language runtime**. The flip side is that AWS does this your behalf, including provisioning capacity, monitoring fleet health, applying security patches, deploying your code, and monitoring and logging your Lambda functions.

### Lambda Function Concurrent Executions
Concurrent executions are the number of executions of your function code at a given point in time. The concurrent execution count will differ depending on whether your Lambda function is processing events from a stream-based event source or not.
Stream-based event sources – If you create a Lambda function that processes events from stream-based services (Amazon Kinesis Streams or DynamoDB streams), the number of shards per stream is the unit of concurrency. So, for example, if you have 10 active shards, there will be 10 Lambda function invocations running concurrently. The Lambda function will process events as they arrive on the stream.

Event sources that are not stream-based – If you create a Lambda function to process events from event sources that are not stream-based such as S3 or API Gateway, each published event is a unit of work. Therefore, the number of events these event sources publish will determine the concurrency

### Error Handling
A Lambda function can fail for any of the following reasons:
* The function times out while trying to reach an endpoint.
* The function fails to successfully parse input data.
* The function experiences resource constraints, such as out-of-memory errors

If any of these failures occur, your function will throw an exception and is handled as follows:
* For non-stream based events – Some of these event sources are set up to invoke a Lambda function synchronously and others invoke it asynchronously.

Depending on how the function is invoked, the errors are handled as follows:
* **Synchronous invocation** – The invoking application receives a 429 error and is responsible for retries. If you invoked the Lambda function directly through AWS SDKs, or through API Gateway, your client receives the error and can choose to retry. If you are invoking Lambda through API Gateway, you need to make sure you map Lambda response errors to API Gateway error codes.

* **Asynchronous invocation** – Asynchronous events are queued before being used to invoke the Lambda function. If AWS Lambda is unable to fully process the event, it will automatically retry the invocation twice, with delays between retries. If you have specified a Dead Letter Queue for your function, then the failed event is sent to the specified Amazon SQS queue or Amazon SNS topic. If you don’t specify a Dead Letter Queue (DLQ), which is not required and is the default setting, then the event will be discarded.

For Stream-based events – For stream-based event sources, AWS Lambda will poll your stream and invoke your Lambda function. `If a Lambda function fails, Lambda attempts to process the error batch of records until the time the data expires`. The exception is treated as blocking, and AWS Lambda will not read any new records from the stream until the failed batch of records either expires or processed successfully. This ensures that AWS Lambda processes the stream events in order.

### Lambda Permissions
* For Lambda to execute your code, you need to ensure that the correct permissions have been granted
* For non-stream based event sources you must grant the event source permissions to invoke your AWS Lambda
* For stream-based event sources, Lambda requires permissions for the relevant stream actions.

When your Lambda function executes, it can access AWS resources in your account such as reading objects in an S3 bucket. AWS Lambda executes your Lambda function on your behalf by assuming the role, you provided at the time of creating the Lambda function.

### IAM Roles
Each Lambda function has an IAM role associated with it and you can grant types of permissions that you grant to the IAM role:
* If the Lambda function need to access resources such as read an object from an S3 bucket you need to grant permissions for relevant Amazon S3
* If the event source is stream-based, AWS Lambda polls these streams on your behalf and thus needs permissions to poll the stream and read new records on the stream so you need to grant the relevant permissions to this role.

### Function Policies
Non-stream-based event sources will invoke your Lambda function if you grant the necessary permissions. So, if you want Amazon S3 to invoke your Lambda function when objects are created in a bucket, then Amazon S3 needs permissions to invoke your Lambda function.  You can grant these permissions via the function policies. AWS Lambda provides APIs for you to manage permission in a functioning policy.

### Access to Resources in a Custom VPC
In most cases, you run workloads in a custom VPC to ensure network isolation and the ability to setup Security Groups and Network Access Control Lists.  AWS Lambda runs your function code in a VPC by default. To use your Lambda function to access resources inside your custom VPC, you must provide your VPC-specific configuration information that includes VPC subnet IDs and security group IDs. AWS Lambda uses this information to set up elastic network interfaces (ENIs) that enable your function to connect securely to other resources within your private VPC.

### Best Practices
The following are recommended best practices for using AWS Lambda:
1. Ensure that your Lambda code is stateless, i.e., there is no affinity between your code and any underlying compute infrastructure.
2. Instantiate AWS clients outside the scope of the handler to take advantage of connection re-use.
3. Make sure you have set read and write permissions on your files in the uploaded ZIP to ensure Lambda can execute code on your behalf.
4. Lower costs and improve performance by minimizing the use of start-up code not directly related to processing the current event.
5. Use the built-in CloudWatch monitoring of your Lambda functions to view and optimize request latencies.
6. Delete old Lambda functions no longer in use.
