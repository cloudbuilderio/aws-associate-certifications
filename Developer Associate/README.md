# Developer Associate #

These are my notes of the acloud.guru DEVELOPER ASSOCIATE course you can find here :  [Certified Developer - Associate 2017](https://acloud.guru/course/aws-certified-developer-associate/dashboard)

**Click** > ["Certified Solutions Architect - Associate 2017 Notes/PDF"](Certified Solutions Architect - Associate 2017.pdf)

Extra Notes :

* [CloudFormation](CloudFormation.md)

* [DynamoDB](DynamoDB.md)

* [Lambda](Lambda.md)

* [SNS](SNS.md)


_For an excellent breakdown of the topics see [Carlo Carbone's](https://bitbucket.org/carlocarbone/aws-certification-study-notes) notes._
